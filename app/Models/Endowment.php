<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Endowment extends Model
{
    use HasFactory;

    protected $fillable = ["category", "endowment_date", "numerical_point", "police", "series_start", "series_end", "effective_date", "quantity_served","stock_quantity", "attestationId","isActive"];

    public function certificate() {
        return $this->belongsTo(Certificate::class, "attestationId", "id");
    }
    public function renewals() {
        return $this->hasMany(Renewal::class, "endowmentId", "id");
    }

   
}
