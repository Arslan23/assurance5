<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Renewal extends Model
{
    use HasFactory;
    protected $fillable = ["date_operation", "insured", "police", "start_date", "end_date", "effective_date", "duration", "brand", "frame", "type", "category", "registration","attestationId", "endowmentId", "isActive"];

    public function certificate() {
        return $this->belongsTo(Endowment::class, "attestationId", "id");
    }

    public function endowment() {
        return $this->belongsTo(Certificate::class, "endowmentId", "id");
    }
}
