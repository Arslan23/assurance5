<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;
    protected $fillable = ["name", "rc", "acces", "taxe", "fga", "total_bonus", "paid_bonus", "isActive"];

    public function endowments() {
        return $this->hasMany(Endowment::class, "attestationId", "id");
    }

    public function renewals() {
        return $this->hasMany(Renewal::class, "attestationId", "id");
    }
}
