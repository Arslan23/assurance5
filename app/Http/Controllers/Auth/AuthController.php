<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function register (Request $request) {
        try
        {
           $user = User::where('username', $request->username)->get();
           $email =  User::where('email', $request->email)->get();
          if (!$user->isEmpty() || !$email->isEmpty() )
          {
              $response = ['message' => 'User already exists'];
              return response()->json($response, 422);

          }
          else{
            $request['password']=Hash::make($request['password']);
            $request['remember_token'] = Str::random(10);
            $user = User::create($request->toArray());
            $tokenSecret = env("PASSPORT_TOKEN_SECRET");
            $token = $user->createToken($tokenSecret)->accessToken;
            $response = ['token' => $token, 'user'=> $user];
            return response()->json($response, 200);
          }
        }
        catch(Exception $e)
        {
            $response = ["message" =>'Error occured'];
            return response()->json($response, 422);
        }
    
    }

    public function login (Request $request) {
        Log::info($request->all());
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()], 422);
        }

        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $tokenSecret = env("PASSPORT_TOKEN_SECRET");
                $token = $user->createToken($tokenSecret)->accessToken;
                $response = ['token' => $token];
                return response()->json($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response()->json($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response()->json($response, 422);
        }
    }

    public function logout (Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response()->json($response, 200);
    }


    public function users()
    {
        $response =  User::all();
        return response()->json($response, 200);
    }

    public function updateUser(Request $request)
    {
      
        $user = $request->all();
        Log::info($user);
        $update =  User::find($user['id']);
        $update->firstname = $user['firstname'];
        $update->lastname = $user['lastname'];
        $update->username = $user['username'];
        $update->email = $user['email'];
        $update->isAdmin = $user['isAdmin'];
        $update->save();
        $response = ['message' => 'You have been successfully update user!', 'data'=> $update];
        return response()->json($response, 200);
    }


    public function removeUser($id)
    {
        Log::info("Remove");
        Log::info($id);
        $user =  User::find($id);
        $user->delete();
        $response = ['message' => 'You have been successfully delete user!'];
        return response()->json($response, 200);
    }

  
}
