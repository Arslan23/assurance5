<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Certificate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class CertificateController extends Controller
{
    public function save (Request $request) {
        try
        {
            Log::info($request->all());
           $certificate = Certificate::create($request->toArray());
            $response = ['message' => "Certificate created successfuly", 'certificate'=> $certificate];
            return response()->json($response, 200);
        }
        catch(Exception $e)
        {
            $response = ["message" =>'Error occured'];
            return response()->json($response, 422);
        }
    
    }

   

 


    public function certificates()
    {
        $response =  Certificate::all();
        return response()->json($response, 200);
    }

    public function updateCertificate(Request $request)
    {
      
        $certificate = $request->all();
        Log::info($certificate);
        $update =  Certificate::find($certificate['id']);
        $update->name = $certificate['name'];
        $update->name = $certificate['rc'];
        $update->name = $certificate['acces'];
        $update->name = $certificate['taxe'];
        $update->name = $certificate['fga'];
        $update->name = $certificate['total_bonus'];
        $update->save();
        $response = ['message' => 'You have been successfully update certificate!', 'data'=> $update];
        return response()->json($response, 200);
    }


    public function removeCertificate($id)
    {
        $certificate =  Certificate::find($id);
        $certificate->delete();
        $response = ['message' => 'You have been successfully delete certificate!'];
        return response()->json($response, 200);
    }

}
