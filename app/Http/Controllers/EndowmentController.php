<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Endowment;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class EndowmentController extends Controller
{
    public function save (Request $request) {
        try
        {
          Log::error($request->all());
          $request['endowment_date'] = date('Y-m-d H:i:s', strtotime($request['endowment_date']));
           $endowment = Endowment::create($request->toArray());
            $response = ['message' => "Endowment created successfuly", 'endowment'=> $endowment];
            return response()->json($response, 200);
        }
        catch(Exception $e)
        {
            $response = ["message" =>'Error occured'];
            return response()->json($response, 422);
        }
    
    }

   

 


    public function endowments()
    {
        $response =  Endowment::with('certificate')->get();
        Log::error($response);
        return response()->json($response, 200);
    }

   /* public function updateEndowment(Request $request)
    {
      
        $endowment = $request->all();
        Log::info($certificate);
        $update =  Endowment::find($certificate['id']);
        $update->name = $certificate['name'];
        $update->name = $certificate['rc'];
        $update->name = $certificate['acces'];
        $update->name = $certificate['taxe'];
        $update->name = $certificate['fga'];
        $update->name = $certificate['total_bonus'];
        $update->save();
        $response = ['message' => 'You have been successfully update certificate!', 'data'=> $update];
        return response()->json($response, 200);
    }*/


    public function removeEndowment($id)
    {
        $endowment =  Endowment::find($id);
        $endowment->delete();
        $response = ['message' => 'You have been successfully delete endowment!'];
        return response()->json($response, 200);
    }

}
