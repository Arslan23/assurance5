<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Endowment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endowments', function (Blueprint $table) {
            $table->id();
            $table->string("category")->nullable();
            $table->date("endowment_date")->nullable();
            $table->string("numerical_point")->nullable();
            $table->string("police")->nullable();
            $table->string("series_start")->nullable();
            $table->string("series_end")->nullable();
            $table->integer("effective_date")->nullable();
            $table->integer("quantity_served")->nullable();
            $table->integer("stock_quantity")->nullable();
            $table->unsignedBigInteger("attestationId")->nullable();
            $table->boolean("isActive")->default(1);
            $table->foreign("attestationId")->references("id")->on("certificates");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endowments');
    }
}
