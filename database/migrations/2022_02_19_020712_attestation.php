<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Attestation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       
    public function up()
    {
            Schema::create('certificates', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->integer("rc")->nullable();
            $table->integer("acces")->nullable();
            $table->integer("taxe")->nullable();
            $table->integer("fga")->nullable();
            $table->integer("total_bonus")->nullable();
            $table->integer("paid_bonus")->nullable();
            $table->boolean("isActive")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
