<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Renewal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renewals', function (Blueprint $table) {
            $table->id();
            $table->date("date_operation")->nullable();
            $table->string("insured")->nullable();
            $table->string("police")->nullable();
            $table->date("start_date")->nullable();
            $table->date("end_date")->nullable();
            $table->integer("effective_date")->nullable();
            $table->integer("duration")->nullable();
            $table->string("brand")->nullable();
            $table->string("frame")->nullable();
            $table->string("type")->nullable();
            $table->integer("category")->nullable();
            $table->integer("registration")->nullable();
            $table->unsignedBigInteger("attestationId")->nullable();
            $table->unsignedBigInteger("endowmentId")->nullable();
            $table->boolean("isActive")->default(1);
            $table->foreign("attestationId")->references("id")->on("certificates");
            $table->foreign("endowmentId")->references("id")->on("endowments");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('renewals', function (Blueprint $table) {
        });
        Schema::enableForeignKeyConstraints();
    }
}
