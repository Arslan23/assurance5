<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\Auth\AuthController;

use App\Http\Controllers\EndowmentController;
use App\Http\Controllers\RenewalController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['auth:api'])->group(function () {
    Route::resource('endowment', 'EndowmentController');
    Route::resource('renewal', 'RenewalController');
    Route::get('/users',  [AuthController::class,'users']);
    Route::put('/user/edit',  [AuthController::class,'updateUser']);
    Route::delete('/user/remove/{id}',  [AuthController::class,'removeUser']);
    Route::get('/certificates',  [CertificateController::class,'certificates']);
    Route::post('/certificate/create',  [CertificateController::class,'save']);
    Route::put('/certificate/edit',  [CertificateController::class,'updateCertificate']);
    Route::delete('/certificate/remove/{id}',  [CertificateController::class,'removeCertificate']);
    Route::post('/endowment/create',  [EndowmentController::class,'save']);
    Route::delete('/endowment/remove/{id}',  [EndowmentController::class,'removeCertificate']);
    Route::get('/endowments',  [EndowmentController::class,'endowments']);
  

    // Utilities
});
Route::post('/login', [AuthController::class,'login'])->name('login');
Route::post('/register',[AuthController::class,'register'])->name('register');
Route::post('/logout', 'AuthController@logout')->name('logout');

